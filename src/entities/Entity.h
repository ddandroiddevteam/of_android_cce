//
// Created by Nilupul Sandeepa on 2021-06-01.
//

#ifndef ENTITY_H
#define ENTITY_H

#include "../utils/Macros.h"
#include "../base/Drawable.h"
#include "../../../../../libs/openFrameworks/types/ofPoint.h"
#include "../utils/Touch.h"

NS_ADE_BEGIN

class Entity: public Drawable {
    public:
        Entity();
        virtual void setup();
        virtual void update();
        virtual void draw();
        virtual void drawUI();

        bool hitTest(const ofPoint& p);

        ofPoint toRelativeCoordinates(const ofPoint& p);
        ofPoint toScreenCoordinates(const ofPoint& p);
        virtual void setZoom(float zoom);

        virtual void onRotation(Touch center, float rotate, EventState state);
        virtual void onPan(Touch translate, EventState state);
        virtual void onPinch(float scale, EventState state);
        virtual void onDrag(Touch poisition, EventState state);

    protected:
        ofPoint position;
        ofPoint dragStart;
        ofPoint rotationCenter;
        ofPoint relativeCenter;

        float width;
        float height;
        float scale;
        float scaleStart;
        float rotation;
        float rotationStart;
        float zoom;
};

NS_ADE_END


#endif //ENTITY_H
