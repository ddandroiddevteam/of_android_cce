//
// Created by Nilupul Sandeepa on 2021-05-29.
//

#include "BlendUtils.h"

NS_ADE_BEGIN

void BlendUtils::clearBuffer(ofFbo &frameBuffer) {
    ofLogError("CCEApp") << "Clearing Buffer";

    frameBuffer.begin();
    ofClear(0, 0, 0, 0);
    frameBuffer.end();
}

NS_ADE_END